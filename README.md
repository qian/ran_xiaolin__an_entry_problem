# 冉晓林-入库题目
冉晓林后端业务题目代码

# 数据
```
-- ----------------------------
-- Table structure for `store_expect`
-- ----------------------------
DROP TABLE IF EXISTS `store_expect`;
CREATE TABLE `store_expect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL COMMENT '仓库ID',
  `expect_stock` int(11) DEFAULT NULL COMMENT '备货数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='预计入库表';

-- ----------------------------
-- Records of store_expect
-- ----------------------------

-- ----------------------------
-- Table structure for `store_sale_day`
-- ----------------------------
DROP TABLE IF EXISTS `store_sale_day`;
CREATE TABLE `store_sale_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL COMMENT '仓库ID',
  `date` date NOT NULL COMMENT '日期',
  `sku` varchar(60) NOT NULL COMMENT '商品SKU',
  `sale_num` int(11) NOT NULL COMMENT '销量',
  `status` tinyint(4) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`),
  KEY `date` (`date`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='日销售统计表';

-- ----------------------------
-- Records of store_sale_day
-- ----------------------------

-- ----------------------------
-- Table structure for `store_stock`
-- ----------------------------
DROP TABLE IF EXISTS `store_stock`;
CREATE TABLE `store_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL COMMENT '仓库',
  `stock` int(11) NOT NULL COMMENT '即时库存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='仓库即时库存标';
```

# 代码逻辑
写在一个文件，提供 StoreService
StoreService::expectNode 即是主要逻辑代码
时间仓促，挺晚了才看到您的回复，2个小时写了以下代码，请见谅。

# 最后
我真诚的对待每一分工作。谢谢~~