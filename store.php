<?php
class StoreService{


    protected $daySale;//日销量
    protected $currentStock;//当前仓库sku商品即时库存

    /**
     * 计算某仓库某某商品 各个入库节点
     * @param $storeId
     * @param $sku
     * @return array
     */
    public function expectNode($storeId, $sku){
        //入库节点配置，数据模拟的,这里为了简便易懂用的拼音key
        $node = [
            'caigou' => 10, //采购期
            'fahuo' => 5,//预计发货日期
            'wuliu' => 6,//物流天数
            //'storage' => 10,//入库
        ];

        $safeDate = 15;//安全库存期
        $this->daySale = $this->getCounter()->getSaleByDate($storeId, $sku);        //日销量
        $this->currentStock = $this->getStock()->findStockByStoreId($storeId, $sku);  //当前仓库sku商品即时库存

        $totalDate = array_sum($node);
        $totalDate += $safeDate;
        $rs = $this->expect($totalDate);
        return $rs;
        /*
        $rs = [];
        $needTime = $safeDate;
        foreach ($node as $key => $value) {
            $needTime += $value; //入库节点加安全库存时间
            $rs[$key] = $this->expect($needTime);
        }
        return $rs;
        */
    }

    /**
     * 预计某仓库某sku商品入库
     * @param int $storeId
     * @param int $sku
     * @param int $jieDian 预计入库节点（按当前时间到节点时间天数算）
     */
    public function expect($jieDian){
        //前提：当前货物足够撑到下次补货入库，否则日销量会出错，平均日销量也会计算错误，需要单独计算，这个后期完善

        $currentStock = $this->currentStock;

        //1.当前库存能支持多久, 当前库存除以日销量
        $nowSafeDate = $currentStock / $daySale; //当前库存能满足的日期

        //2.在当前库存使用完前，需要补货，备货数量要满足  从备货到入库节点
        //不考虑其他情况下的，当前库存为0的情况下，从发货到入库节点
        $need = $jieDian - $nowSafeDate;//总共需要的数量减去当前库存 就是预计入库时间内需要的备货量

        //3.在这个过程中，可能有货物已经在备货过程或者发货过程
        //查找是否在当前时间到入库节点时间 是否有入库
        $hasStoreNums = 99; //这个就查询备货表，这个不在这里写了,例如查出来是99
        $realNeed = $need - $hasStoreNums;


        return $realNeed;//这个就是当前时间到入库时间节点 需要的备货量
    }

    //简易处理下事件监听, 对应的方法问Even.. 例如out对应 EventOut
    protected $listener = [
        'store.out' => 'out',
        'store.storage' => 'storage',
    ];

    //触发事件
    protected function trigger($event){
        //触发事件，参数由func_get_args获取
        //触发上面listener相应的相应函数
    }

    public function getStock(){
        return new StoreStock();
    }

    public function getCounter(){
        return new StoreCounter();
    }

    /**
     * 出库，这里会增加减少库存
     * 这里理解为销售一个商品，就视为出库一个，销售多少，就是出库多少，当天出库视为当天的销量
     * @param $storeId
     * @param $sku
     * @param $nums
     */
    public function out($storeId, $sku, $nums){
        $rs = $this->getStock()->descStock($storeId, $sku, $nums);
        if($rs){
            $this->trigger('store.out', [$storeId, $sku, $nums]);
        }
    }

    /**
     * 入库，增加即使库存
     * @param $storeId
     * @param $sku
     * @param $nums
     */
    public function storage($storeId, $sku, $nums){
        $rs = $this->getStock()->ascStock($storeId, $sku, $nums);
        if($rs){
            $this->trigger('store.storage', [$storeId, $sku, $nums]);
        }
        //..
    }

    /**
     * 出库监听
     * @param $storeId
     * @param $sku
     * @param $nums
     */
    protected function EventOut($storeId, $sku, $nums){
        //出库后增加日销售统计表数据
        $this->getCounter()->addStockToStoreDay($storeId, $sku, $nums);
    }

}

Class ModelBase{
    public function findOne($id){return [];}
    public function findAll($cond){return [];}
    public function update($id, $post, $cond = []){return true;}
    public function insert($post){return rand();}
    public function delete($id, $cond = []){return true;}
}

class StoreBase{
    protected $model;

    protected function getModel(){
        return $this->model ?: $this->model = new ModelBase();
    }
}

class StoreStock extends StoreBase {

    protected $stock;
    /**
     * 根据仓库id sku商品 获取即时库存
     * @param $id
     */
    public function findStockByStoreId($id, $sku){
        $stockModel = $this->getModel()->findOne($id); //从数据库查出的
        $this->stock = $stockModel;
        return;
    }

    /**
     * 增加当前库存
     */
    public function ascStock($storeId, $sku, $nums){}

    /**
     * 减少当前库存
     */
    public function descStock($storeId, $sku, $nums){}

}

/**
 * 仓库商品日销量统计
 * Class StoreCounter
 */
class StoreCounter extends StoreBase {
    /**
     * 获取某天某仓库某商品的销量
     * @param $id
     * @param $date
     */
    public function findStockByStoreIdDay($storeId, $date, $sku){
    }

    /**
     * 增加某天某仓库某商品的销量
     * @param int $storeId
     * @param string $date
     * @param string $sku
     * @param int $sales
     */
    public function addStockToStoreDay($storeId, $sku, $sales){
        //这里检查此仓库 sku商品是否有销量，有则累加sale_num字段，没有则新增一条记录
    }

    protected $stepDay = 28;
    /**
     * 读取近stepDay天的销售情况，获取平均值，
     */
    public function getSaleByDate($storeId, $sku){

    }
}
